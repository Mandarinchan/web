package ru.zsrc.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.zsrc.demo.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(Long id);
}
