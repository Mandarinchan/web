package ru.zsrc.demo.service.impl;

import ru.zsrc.demo.model.User;
import ru.zsrc.demo.repository.UserRepository;
import ru.zsrc.demo.service.UserService;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findUserById(Long userId) {
        return userRepository.findOne(userId);
    }

    public Optional<User> getById(Long id) {
        return userRepository.findById(id);
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public User saveUser(User app) {
        return userRepository.save(app);
    }
}
