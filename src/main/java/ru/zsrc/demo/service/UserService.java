package ru.zsrc.demo.service;

import ru.zsrc.demo.model.User;

import java.util.List;

public interface UserService {

    User findUserById(Long userId);

    List<User> getAllUser();

    User saveUser(User application);
}
