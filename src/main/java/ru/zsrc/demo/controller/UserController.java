package ru.zsrc.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.zsrc.demo.model.User;
import ru.zsrc.demo.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/rest/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping("/get")
    public @ResponseBody String test() {
        return userService.findUserById(1L).getFirstName();
    }

    @RequestMapping("user")
    public List<User> getAllApplications() {
        List<User> applications = userService.getAllUser();
        return applications;
    }

    @ResponseBody
    public User saveUser(@RequestBody User application) {
        return userService.saveUser(application);
    }
}
